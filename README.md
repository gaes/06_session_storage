# The sessionStorage Object #

The sessionStorage object is equal to the localStorage object, except that it stores the data for only one session. The data is deleted when the user closes the specific browser tab.
The following example counts the number of times a user has clicked a button, in the current session:

https://www.w3schools.com/html/tryit.asp?filename=tryhtml5_webstorage_session